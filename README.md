Limpet
======

This is a python 3 script that helps the default python interpreter behave like a command line.

It relies mostly on the excellent sh library for python, which allows you to make system calls as though they
were python functions.
```python
ls()
cat("file.txt")
```

Limpet will find and define every executable in your path, with a few exceptions (for example, if you have a
`/bin/dir`, it will not be imported, as it would overwrite the python builtin function `dir()`). Commands with dashes
in their name, like `apt-get`, will be imported as e.g. `apt_get`. Commands that begin with a number, or use
forbidden characters in python, like  ".", will not be imported.

To execute these unimported commands, as well as other executable files, use the `cmd()` function:
```python
cmd("mkfs.ext4")(argument) # illegal python function name
cmd("/home/user/scripts/foo.sh")(argument) # random executable file
```

You may also sometimes want to use commands as objects before executing them. To do this, simply do not call them
and treat them like normal python function objects.

```python
do_stuff(ls) # ls is not executed yet
# ls can be executed inside do_stuff when it is ready
# perhaps it does a git clone or untars something first
```

You can also use the sh library's `bake()` method to provide function arguments ahead of time, without executing:

```python
do_stuff(ls.bake("../foo/")) # ls is not executed yet
# ls can be executed with the given path by just calling argument() inside do_stuff
```

Commands execute in "foreground mode" by default, making them behave like a normal shell command. In some cases,
however, you may want the sh library's default behavior, which treats these commands similar to python's native
subprocess handling and allows you access to e.g. the exit code after a command completes. To obtain this
representation, enclose the command in underscores:

```python
ls("foo/")
_ls_("foo/").exit_code
_cmd_("mkfs.ext4").exit_code
```

Some other provided functions:

```python
out(_ls_("foo/")) # parse command output into a tuple of strings and strip trailing newlines
```
```python
empty(_command_()) # return true if command's output is empty
```
```python
ok(_command_()) # convert from 0-means-success return code to boolean
```
```python
chain(_ls_.bake("../foo/"),_pwd_) # equivalent to 'ls ../foo/ && pwd'
# stops execution of a set of commands if one returns an error
```
```python
_chain_(_ls_.bake("../foo/"),_pwd_) # same as above, but returns all of the command objects,
# any exception object thrown, or None for commands that were not run
```
```python
alt(_ls_.bake("../foo/"),_pwd_) # equivalent to 'ls ../foo/ || pwd'
# stops execution of a set of commands on the first success
```
```python
_alt_(_ls_.bake("../foo/"),_pwd_) # same as above, but returns all of the command objects,
# any exception objects thrown, or None for commands that were not run
```

You also get a nice (in my opinion) PS1. If you don't like it (or anything else about limpet's default
environment), create ~/.limpetrc.py . In here, you can import python libraries you want to have
available when you launch your shell, define aliases (by declaring global variables), create your own
python functions you want to be able to run like programs, etc.

```python
import sys

# custom functions
def ll(*args,**kwargs):
    # no need to return normal (foreground) commands
    ls(*args,l=True,**kwargs)
def _ll_(*args,**kwargs):
    # return non-foreground commands so you can get their object
    return _ls_(*args,l=True,**kwargs)

# or just do this for easier aliases
la=ls.bake(a=True)
_la_=_ls_.bake(a=True)

class MyPs1:
    def __repr__(self):
        # be sure to use stylize_interactive when you're in the PS1 to avoid weird readline errors
        return color.stylize_interactive("python> ",color.fg("light_green"))

sys.ps1=MyPs1()
sys.ps2=">>>>>>> "

del sys # don't want sys on my command line
```

CLI Syntax
----------
Limpet's valid syntax is almost the same as the standard python interactive mode, with one major exception: If
you give the interpreter just an executable object, such as a function or shell command, with no parentheses,
the interpreter will call that object with no arguments. This makes certain operations easier to type:

```python
ls # equivalent to ls()
git.push # equivalent to git.push()
ls("./foo") # when you need to provide an argument
```

If this makes you uncomfortable, you can turn it off by setting `_limpet_flags_auto_invoke=False`, e.g. in
your `~/.limpetrc.py`.

Directory System
----------------
Python's interpreter supports tab completion of things it knows about, such as functions. It, of course,
cannot tab-complete filesystem paths for you, because these paths exist in strings in limpet, and
tab-completion within an arbitrary string has no meaning.

To resolve this issue and allow tab-completion of paths, limpet has a directory system. Three starting
points are provided, and each starting point object has attributes corresponding to its directories.

```python
cwd.foo # corresponds to ./foo
root.usr.bin # corresponds to /usr/bin
home.Documents # corresponds to ~/Documents
```

Entering just a directory on the command line will cd to that directory. Passing a directory to a function
or executable where a string is expected will transform it into the normal string representation of that
directory path (relative path for cwd, absolute for the other two).

The parent directory of a directory in this system is named `__` (two underscored), since the traditional
`..` is not a valid python function name.

```python
cwd.__ # corresponds to ./..
cwd.__.__ # corresponds to ./../..
```

You can tab-complete these directories from the interpreter, and the tab-completion will additionally show
files in the current directory, in the form

```python
cwd.foo+"file.txt"
```

Unfortunately, you must type the filename yourself. You will also need to handle directory names containing
period characters specially, as well as any other character that is filesystem-valid but not a valid character
for a python object name.

```python
root.etc.apt+"sources.list.d/" # this directory contains periods, and so would be confusing as attributes
```

If you do not have the working directory in your ps1, you may wish to set `_limpet_flags_print_wd=True` in
your `~/.limpetrc.py`. This will print the actual working directory whenever a directory object is used to
change it (when auto-invoked).

But Y Tho?
----------
Because I hate shell scripting and wish it would die in a fire.

Python may be a bit more of a pain in cases like
```python
cat("file.txt") # 3 extra chars, and 4 total are harder to type than shell
```
but the tradeoff of having sane support of lists/tuples/dicts and modern/fancy features like list comprehension
right in your command line makes that more than worth it, in my opinion. Plus, no horrible POSIX syntax.

Why Not a Custom Syntax?
------------------------
i.e. why didn't I write my own parser for a shell-like hybrid language just so I can have both python code and say `cd ..`?
One, that would be hard and introduce possible edge cases. Two, one of the things I hate about shell code is its
difficulty handing the ambiguity between strings and commands, i.e. the difference between `do_stuff $arg` and `do_stuff "$arg"`
if `$arg` contains a space. Stock python syntax does not have this ambiguity, removing a whole class of sometimes
very dangerous problems from your shell, at the expense of getting more familiar with your parentheses and quote keys
than you perhaps wanted.

Python 2?
---------
No. [Come on, guys.](https://www.python.org/dev/peps/pep-0404/#and-now-for-something-completely-different)

Other Interpreters
------------------
Limpet is customized for the default interactive python shell. The only other interpreter I've gotten it
working in is IPython, which turned out to be a clusterfuck, so I dropped support for that. It's known to
not work in BPython.

I'll be looking to improve the native interactive mode rather than adding support for alternate interpreters.

Installing
----------
Download `limpet` and put it somewhere like `~/bin`. Or `/usr/bin` if you're brave.
I wouldn't `chsh -s /usr/bin/limpet`. This thing is pretty alpha.

Make sure you actually have the python3 executable installed, of course, and additionally have done
`pip3 install sh colored`
